import React from "react";
const isHookName = (hookKey) => {
  if (!hookKey.startsWith("use"))
    return false;
  const startOfSecondWord = hookKey.charAt(3);
  const isCapital = startOfSecondWord.toUpperCase() === startOfSecondWord;
  return isCapital;
};
const isObject = (x) => typeof x === "object" && x !== null;
const spreadInto = (props, obj) => Object.entries(obj).reduce(
  (acc, [key, val]) => ({
    ...acc,
    [key]: val
  }),
  props
);
const parseHookKey = (hookKey) => {
  const [rest, overrideIfNotUndefined] = hookKey.split("!");
  const tokens = rest.split(">");
  const [token1, token2] = tokens;
  const name = token2 || token1;
  const injectProps = tokens.length === 2;
  return { name, injectProps, override: overrideIfNotUndefined !== void 0 };
};
export const composeHooks = (hooks) => (Component) => {
  if (!Component)
    throw Error("Component must be provided to compose");
  if (!hooks)
    return Component;
  return (props) => {
    const hooksObject = typeof hooks === "function" ? hooks(props) : hooks;
    const { hooksProps, overrideProps } = Object.entries(hooksObject).reduce(
      (acc, [hookKey, hook]) => {
        const { name, override, injectProps } = parseHookKey(hookKey);
        const shouldOverride = override || Array.isArray(hook);
        const hookFn = Array.isArray(hook) ? hook[0] : hook;
        const hookValue = hookFn(
          injectProps ? { ...acc.hooksProps, ...props, ...acc.overrideProps } : void 0
        );
        const shouldSpread = isObject(hookValue) && isHookName(name);
        const propsKey = shouldOverride ? "overrideProps" : "hooksProps";
        const propsVal = acc[propsKey];
        return {
          ...acc,
          [propsKey]: shouldSpread ? spreadInto(propsVal, hookValue) : name ? { ...propsVal, [name]: hookValue } : propsVal
        };
      },
      { hooksProps: {}, overrideProps: {} }
    );
    return /* @__PURE__ */ React.createElement(Component, { ...hooksProps, ...props, ...overrideProps });
  };
};
export default composeHooks;
