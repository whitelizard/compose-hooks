import React from 'react';

const isHookName = (hookKey) => {
  if (!hookKey.startsWith('use')) return false;
  const startOfSecondWord = hookKey.charAt(3);
  const isCapital = startOfSecondWord.toUpperCase() === startOfSecondWord;
  return isCapital;
};

// eslint-disable-next-line no-null/no-null
const isObject = (x) => typeof x === 'object' && x !== null;

const spreadInto = (props, obj) =>
  Object.entries(obj).reduce(
    (acc, [key, val]) => ({
      ...acc,
      [key]: val,
    }),
    props,
  );

/**
 * Check if a "hook key" contains any of the operators:
 * - "!" last means override.
 * - ">" first means injectProps.
 * @func
 * @param {string} hookKey - A key given to composeHooks object.
 * @returns {Object} - name, injectProps, override.
 */
const parseHookKey = (hookKey) => {
  const [rest, overrideIfNotUndefined] = hookKey.split('!');
  const tokens = rest.split('>');
  const [token1, token2] = tokens;
  const name = token2 || token1;
  const injectProps = tokens.length === 2;
  return { name, injectProps, override: overrideIfNotUndefined !== undefined };
};

/**
 * Preconfigured HOC (Higher Order Component, that takes a Component and returns
 * a Component) that binds functions and hooks into a wrapped component.
 * As described also in parseHookKey above, the hook key syntax/features are:
 * - "!" last means the prop will override possible incomming prop with the same name.
 * - ">" first means that all available props will be injected into the function/hook.
 * @func
 * @param {Object|Function} hooks - Collection of hooks parsed and executed by the resulting function.
 * @returns {Function} - HOC
 */
export const composeHooks = (hooks) => (Component) => {
  // eslint-disable-next-line functional/no-throw-statements
  if (!Component) throw Error('Component must be provided to compose');
  if (!hooks) return Component;

  return (props) => {
    const hooksObject = typeof hooks === 'function' ? hooks(props) : hooks;

    // Flatten values from all hooks to a single object
    const { hooksProps, overrideProps } = Object.entries(hooksObject).reduce(
      (acc, [hookKey, hook]) => {
        const { name, override, injectProps } = parseHookKey(hookKey);
        const shouldOverride = override || Array.isArray(hook);
        const hookFn = Array.isArray(hook) ? hook[0] : hook;
        const hookValue = hookFn(
          injectProps ? { ...acc.hooksProps, ...props, ...acc.overrideProps } : undefined,
        );
        // If hookValue is object and key starts with useX (is a custom hook), unpack/spread!
        const shouldSpread = isObject(hookValue) && isHookName(name);
        const propsKey = shouldOverride ? 'overrideProps' : 'hooksProps';
        const propsVal = acc[propsKey];
        return {
          ...acc,
          [propsKey]: shouldSpread
            ? spreadInto(propsVal, hookValue)
            : name
            ? { ...propsVal, [name]: hookValue }
            : propsVal,
        };
      },
      { hooksProps: {}, overrideProps: {} },
    );

    return <Component {...hooksProps} {...props} {...overrideProps} />;
  };
};

export default composeHooks;
